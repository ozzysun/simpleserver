mysql = require 'mysql'
express = require 'express'
cors = require 'cors'
path = require 'path' 
class Tool
  constructor:(@name)->    
  app:null
  staticPath:null
  trace:(str)=>
    console.log str
  # mysql  
  connectDB:(_config)=>
    connection = mysql.createConnection _config
    connection.connect (err)=>
      if err                                    
        console.log 'error when connecting to db:', err
        setTimeout =>
          @connectDB _config
        , 2000
    connection.on 'error', (err)=>
      console.log 'db error', err
      if err.code is 'PROTOCOL_CONNECTION_LOST' 
        @connectDB _config                      
      else                                     
        throw err                                  
     
  # for express old --
  getApp:(_port,_staticPath=null)=>
    app = express()
    app.use cors()
    app.configure ->
      app.use express.bodyParser()
      app.use app.router
    @trace '[CRM_SERVER] Listening on prot:'+ "#{_port}" +'...ctr+c to stop service'
    app.listen _port
    @app = app
    if _staticPath is null
      @setStaticPath()
    app
  # for express 4 --  
  getRouter4:(_port,_staticPath=null)=>
    bodyParser = require 'body-parser'
    app = express()
    app.use cors()
    app.use bodyParser.urlencoded
      extended: true
    app.use bodyParser.json()
    router = express.Router()
    app.use "/",router
    app.listen _port,()->
      console.log 'express ready'
    @app = app
    if _staticPath is null
      @setStaticPath()
    else
      @setStaticPath _staticPath
    router
  setStaticPath:(_path="./public")=>
    @staticPath = path.resolve(_path)
    @app.use express.static(@staticPath)
tool = new Tool 'ozzysun'
module.exports = tool 
