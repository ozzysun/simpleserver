# Get the directory that this configuration file exists in
dir = File.dirname(__FILE__)

# Compass configurations
sass_path = File.join("www","src","scss")
css_path = File.join("www","css")
images_dir = File.join("www","images")

# Require any additional compass plugins here.
#output_style = :compressed
#environment = :production
output_style = :expanded
environment = :development